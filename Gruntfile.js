/**
  * todo:
  * 1st time: create the repo ./dist, and an file "/config/project.private.json" with "{}" inside
  * When working: execute "grunt"
  */

var fs = require('fs');

//var cfgPrivate = require('./config/project.private.json');
var cfgPublic  = require('./config/project.public.json');

module.exports = function(grunt) {

  //Get local network ip
  var os=require('os');
  var ifaces=os.networkInterfaces();
  var lookupIpAddress = null;
  for (var dev in ifaces) {
      if(dev != "en1" && dev != "en0") {
        //continue;
      }
      ifaces[dev].forEach(function(details){
        if (details.family=='IPv4') {
          lookupIpAddress = details.address;
          return;
        }
      });
  }

  //If an IP Address is passed
  //we're going to use the ip/host from the param
  //passed over the command line
  //over the ip addressed that was looked up
  var ipAddress        = grunt.option('host') || lookupIpAddress;
  var preprocess_files = {
    'app/js/config.js': 'config/config.tmpl.js'
  };

  var envs      = ['dev', 'stage', 'live'];
  var targetenv = 'dev';
  var ary       = grunt.cli.tasks[0] ? grunt.cli.tasks[0].split('-') : [];
  if(ary.length >1 && envs.indexOf(ary[1])>-1){
    targetenv = ary[1];
  }
  console.log('**** targetenv:'+targetenv);
  var suffix = '-'+targetenv+'-'+cfgPublic.env[targetenv].version;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    //testflight_settings: cfgPublic.testflight.settings,
    preprocess: {
      dev: {
        files : preprocess_files
      },
      stage: {
        files : preprocess_files
      },
      live: {
        files : preprocess_files
      }
    },
    env: {
      dev: {
        NODE_ENV: 'DEV',
        IP_ADDRESS: ipAddress,
        APP_NAME: cfgPublic.env.dev.name,
        APP_VERSION: cfgPublic.env.dev.version,
        LOG: true
      },
      stage: {
        NODE_ENV: 'STAGE',
        IP_ADDRESS: ipAddress,
        APP_NAME: cfgPublic.env.stage.name,
        APP_VERSION: cfgPublic.env.stage.version,
        LOG: true
      },
      live: {
        NODE_ENV: 'LIVE',
        APP_NAME: cfgPublic.env.live.name,
        APP_VERSION: cfgPublic.env.live.version,
        LOG: false
      }
    },
    sass: {
      dist: {
        files: {
          'app/css/app.css' : 'app/sass/app.scss'
        }
      }
    },
    watch: {
      css: {
        files: 'app/*/*.scss',
        tasks: ['sass']
      },
      tmpl: {
        files: 'config/**/*.tmpl.*',
        tasks: ['env:dev', 'preprocess:dev']
      },
      templates: {
        files: 'app/pages/**/*.html',
        tasks: ['html2js']
      }
    },
    connect: {
      server: {
        options: {
          port: 8081,
          base: '',
          //keepalive: true,
          livereload: true
        }
      }
    },
    cssmin: {
      minify: {
        expand: true,
        cwd: 'app/css/',
        src: ['*.css'],
        dest: 'build/css/',
        ext: '.min.css'
      }
    },
    concat: {
      options: {
        separator: ';\n'
      },
      build: {
        files: [{
          src: [
          'app/lib/socket.io.js',
          'app/lib/sails.io.js',
          'app/lib/underscore-min.js',
          'app/lib/jquery.min.js',
          /*'app/lib/angular/angular.js',
          'app/lib/angular/angular-touch.js',
          'app/lib/angular/angular-animate.js',*/
          'app/lib/angular.1.1.5.js',
          'app/lib/angular-ui/ui-utils/event/event.js',
          'app/lib/angular-ui/ui-map.js',
          'app/js/config.js',
          'app/js/**/*.js'
          ],
          dest: 'temp/app.js'
        }]
      },
      live: {
        files: [{
          src: [
          'app/lib/socket.io.js',
          'app/lib/sails.io.js',
          'app/lib/underscore-min.js',
          'app/lib/jquery.min.js',
          /*'app/lib/angular/angular.js',
          'app/lib/angular/angular-touch.js',
          'app/lib/angular/angular-animate.js',*/
          'app/lib/angular.1.1.5.js',
          'app/lib/angular-ui/ui-utils/event/event.js',
          'app/lib/angular-ui/ui-map.js'
          ],
          dest: 'temp/lib.js'
        },
        {
          src: [
          'app/js/config.js',
          'app/js/**/*.js'
          ],
          dest: 'temp/core.js'
        }]
      },
      livefinal: {
        files: [{
          src: [
          'temp/lib.js',
          'temp/core.ob.js'
          ],
          dest: 'temp/app.js'
        }]
      }
    },
    uglify: {
      build: {
        src: 'temp/app.js',
        dest: 'build/js/app.min.js'
      }
    },
    copy: {
      build: {
        files: [{
          expand: true,
          src: ['config.xml'],
          dest: 'build/',
          cwd: 'app/'

        },/*{
          expand: true,
          src: ['**'],
          dest: 'build/pages',
          cwd: 'app/pages'
        },*/{
          expand: true,
          src: ['**'],
          dest: 'build/img/',
          cwd: 'app/img/'

        },{
          expand: true,
          src: ['**'],
          dest: 'build/lib/font-awesome',
          cwd: 'app/lib/font-awesome'
        },{
          expand: true,
          src: ['**'],
          dest: 'build/fonts/',
          cwd: 'app/fonts/'
        },{
          expand: true,
          src: ['**'],
          dest: 'build/splash/',
          cwd: 'app/splash/'
        },{
          expand: true,
          src: ['**'],
          dest: 'build/icons/',
          cwd: 'app/icons/'
        },{
          expand: true,
          src: ['**'],
          dest: 'build/locales/',
          cwd: 'app/locales/'
        }]
      },
      forstage: {
        files: [{
          expand: true,
          src: ['app.js'],
          dest: 'build/js/',
          cwd: 'temp/'

        }]
      },
      fordev: {
        files: [{
          expand: true,
          src: ['app.css'],
          dest: 'build/css/',
          cwd: 'app/css/'

        },{
          expand: true,
          src: ['**'],
          dest: 'build/js/',
          cwd: 'app/js/'

        },{
          expand: true,
          src: ['**'],
          dest: 'build/lib/',
          cwd: 'app/lib/'

        },{
          expand: true,
          src: ['**'],
          dest: 'build/img/',
          cwd: 'app/img/'

        },{
          expand: true,
          src: ['**'],
          dest: 'build/fonts/',
          cwd: 'app/fonts/'
        },{
          expand: true,
          src: ['**'],
          dest: 'build/splash/',
          cwd: 'app/splash/'
        },{
          expand: true,
          src: ['**'],
          dest: 'build/icons/',
          cwd: 'app/icons/'
        }]
      }
    },
    html2js: {
      options: {
        base: '',
        module: 'app.templates'
      },
      main: {
        src: ['app/pages/**/*.html'],
        dest: 'app/js/templates.js'
      },
    },
    buildenv: {
      index: {
        src: 'app/index.html',
        dest: 'build/index.html'
      },
      configxml: {
        src: 'app/config.xml',
        dest: 'build/config.xml'
      }
    },
    clean: ["temp/*", "build/**/*", "dist/*"],
    // inline_angular_templates: {
    //   build: {
    //     options: {
    //       base: 'app', // (Optional) ID of the <script> tag will be relative to this folder. Default is project dir.
    //       selector: 'body',       // (Optional) CSS selector of the element to use to insert the templates. Default is `body`.
    //       method: 'prepend'       // (Optional) DOM insert method. Default is `prepend`.
    //     },
    //     files: {
    //       'build/index.html': ['app/pages/**/*.html']
    //     }
    //   }
    // },
    compress: {
      main: {
        options: {
          archive: 'temp/app'+suffix+'.zip'
        },
        files: [
          { expand: true, src: "**/*", cwd: "build/"}
        ]
      }
    },
    // 'phonegap-build': {
    //   dev: {
    //     options: {
    //       archive: '<%= compress.main.options.archive %>',
    //       appId: cfgPublic.phonegap['dev-appId'],
    //       /*timeout: 120000,*/
    //       pollRate: 10,
    //       user: cfgPrivate.phonegap.user,
    //       keys: cfgPrivate.phonegap.keys,
    //       download: {
    //         ios: 'dist/<%= pkg.name %>'+suffix+'.ipa',
    //         android: 'dist/<%= pkg.name %>'+suffix+'.apk'
    //       }
    //     }
    //   },
    //   stage: {
    //     options: {
    //       archive: '<%= compress.main.options.archive %>',
    //       appId: cfgPublic.phonegap['stage-appId'],
    //       /*timeout: 120000,*/
    //       pollRate: 10,
    //       user: cfgPrivate.phonegap.user,
    //       keys: cfgPrivate.phonegap.keys,
    //       download: {
    //         ios: 'dist/<%= pkg.name %>'+suffix+'.ipa',
    //         android: 'dist/<%= pkg.name %>'+suffix+'.apk'
    //       }
    //     }
    //   },
    //   live: {
    //     options: {
    //       archive: '<%= compress.main.options.archive %>',
    //       appId: cfgPublic.phonegap['live-appId'],
    //       /*timeout: 120000,*/
    //       pollRate: 10,
    //       user: cfgPrivate.phonegap.user,
    //       keys: cfgPrivate.phonegap.keys,
    //       download: {
    //         ios: 'dist/<%= pkg.name %>'+suffix+'.ipa',
    //         android: 'dist/<%= pkg.name %>'+suffix+'.apk'
    //       }
    //     }
    //   }
    // },
    // shell: {
    //   'testflight-dev': {
    //     options: {
    //       stdout: true
    //     },
    //     command: [
    //       'cd dist',
    //       "curl http://testflightapp.com/api/builds.json " +
    //       "-F file=@<%= pkg.name %>"+suffix+".ipa " +
    //       "-F api_token='<%= testflight_settings.apiToken %>' " +
    //       "-F team_token='<%= testflight_settings.teamToken %>' " +
    //       "-F notes='Some notes for automated upload' " +
    //       "-F notify=False " +
    //       "-F distribution_lists='teammates' "/*,
    //       "curl http://testflightapp.com/api/builds.json " +
    //       "-F file=@<%= pkg.name %>"+suffix+".apk " +
    //       "-F api_token='<%= testflight_settings.apiToken %>' " +
    //       "-F team_token='<%= testflight_settings.teamToken %>' " +
    //       "-F notes='Some notes for automated upload' " +
    //       "-F notify=False " +
    //       "-F distribution_lists='teammates' "*/
    //     ].join("&&")
    //   },
    //   'testflight-stage': {
    //     options: {
    //       stdout: true
    //     },
    //     command: [
    //       'cd dist',
    //       "curl http://testflightapp.com/api/builds.json " +
    //       "-F file=@<%= pkg.name %>"+suffix+".ipa " +
    //       "-F api_token='<%= testflight_settings.apiToken %>' " +
    //       "-F team_token='<%= testflight_settings.teamToken %>' " +
    //       "-F notes='Some notes for automated upload' " +
    //       "-F notify=False " +
    //       "-F distribution_lists='teammates' "/*,
    //       "curl http://testflightapp.com/api/builds.json " +
    //       "-F file=@<%= pkg.name %>"+suffix+".apk " +
    //       "-F api_token='<%= testflight_settings.apiToken %>' " +
    //       "-F team_token='<%= testflight_settings.teamToken %>' " +
    //       "-F notes='Some notes for automated upload' " +
    //       "-F notify=False " +
    //       "-F distribution_lists='beta' "*/
    //     ].join("&&")
    //   },
    //   'testflight-beta': {
    //     options: {
    //       stdout: true
    //     },
    //     command: [
    //       'cd dist',
    //       "curl http://testflightapp.com/api/builds.json " +
    //       "-F file=@<%= pkg.name %>"+suffix+".ipa " +
    //       "-F api_token='<%= testflight_settings.apiToken %>' " +
    //       "-F team_token='<%= testflight_settings.teamToken %>' " +
    //       "-F notes='Some notes for automated upload' " +
    //       "-F notify=False " +
    //       "-F distribution_lists='beta' "/*,
    //       "curl http://testflightapp.com/api/builds.json " +
    //       "-F file=@<%= pkg.name %>"+suffix+".apk " +
    //       "-F api_token='<%= testflight_settings.apiToken %>' " +
    //       "-F team_token='<%= testflight_settings.teamToken %>' " +
    //       "-F notes='Some notes for automated upload' " +
    //       "-F notify=False " +
    //       "-F distribution_lists='beta' "*/
    //     ].join("&&")
    //   }
    // }
  });

  grunt.registerMultiTask('buildenv', 'Build env specific files', function() {
    var done = this.async();
    var options = this.options();
    var files = this.files;

    grunt.util.async.forEachSeries(this.files, function(f, nextFileObj) {
      var destFile = f.dest;
      var sourceFile = f.src[0];

      var pattern = new RegExp( "<!--\\["+targetenv+"\\](--)*?>((.|\\n)*?)<!(--)*\\[end"+targetenv+"\\]-->", "gi" );
      var others  = [];
      for(var key in envs){
        if(envs[key]!=targetenv){
          others.push(new RegExp( "<!--\\["+envs[key]+"\\](--)*?>((.|\\n)*?)<!(--)*\\[end"+envs[key]+"\\]-->","gi"));
        }
      }

      var paramPat = /{{(.+)}}/gi;
      var indexContent = fs.readFileSync(sourceFile, 'utf-8');
      indexContent = indexContent.replace(pattern, function($1, $2, $3) {
        //evaluate parameters
        var evalParams = $3;
        return evalParams.replace(paramPat, function(t){
          return eval(t.substr(2,t.length-4)) || '';
        });
      });
      for(var i=0, l=others.length; i<l; ++i){
        (function(regex){
          indexContent = indexContent.replace(regex, function(){
            return '';
          });
        })(others[i]);
      }
      indexContent = indexContent.replace(/\s{2,}/gi, ' ');
      var ret = fs.writeFileSync(destFile, indexContent);
      console.log(ret);
      done();
    });
  });

  //load grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.registerTask('default',['env:dev', 'preprocess:dev', 'sass', 'html2js', 'connect:server', 'watch']);

  grunt.registerTask('dev-prebuild',['clean', 'sass', 'html2js', 'copy:fordev', 'buildenv', 'compress']);
  grunt.registerTask('common-prebuild',['clean', 'sass', 'cssmin', 'html2js', 'concat:build', 'copy:build', 'copy:forstage', 'buildenv', 'compress']);
  grunt.registerTask('live-prebuild',['clean', 'sass', 'cssmin', 'html2js', 'concat:build', 'uglify', 'copy:build', 'buildenv', 'compress']);

  grunt.registerTask('build-dev', ['env:dev', 'preprocess:dev', 'dev-prebuild']);
  grunt.registerTask('build-stage', ['env:stage', 'preprocess:stage', 'common-prebuild']);
  grunt.registerTask('build-live', ['env:live', 'preprocess:live', 'live-prebuild']);

  grunt.registerTask('distrib-dev',['build-dev', 'phonegap-build:dev', 'shell:testflight-dev']);
  grunt.registerTask('distrib-stage',['build-stage', 'phonegap-build:stage', 'shell:testflight-stage']);
  grunt.registerTask('distrib-live',['build-live', 'phonegap-build:live', 'shell:testflight-beta']);
};
