describe("String", function() {

  describe("startsWith (requirement5)", function() {

    beforeEach(function() {
      sample = 'hang the dj';
    });

    var tests = [
      {entry:'hang', expect: true},
      {entry:'Hang', expect: false},
      {entry:"I've got a room for rent", expect: false},
      {entry:'', expect: true},
      {entry:42, expect: false},
      {entry:{ first: 'Johnny'}, expect: false}
    ];

    tests.forEach(function(test){
      var spec = "Should ";
      spec += test.expect ? "":"not ";
      spec += "start with " + JSON.stringify(test.entry);

      it(spec, function() {
        expect( sample.startsWith(test.entry) ).toBe(test.expect);
      });
    });

  });

  describe("endsWith (requirement5)", function() {

    var sampleSrc = 'hang the dj';
    beforeEach(function() {
      sample = sampleSrc;
    });

    var tests = [
      {entry:'dj', expect: true},
      {entry:'panic on the streets', expect: false},
      {entry:'', expect: true},
      {entry:'the dj', expect: true},
      {entry:42, expect: false},
      {entry:{ first: 'Johnny'}, expect: false},
      {entry:{ first: 'the '}, expect: false}
    ];

    tests.forEach(function(test){
      var spec = "Should '"+sampleSrc+"'";
      spec += test.expect ? "":"not";
      spec += " end with " + JSON.stringify(test.entry);

      it(spec, function() {
        expect( sample.endsWith(test.entry) ).toBe(test.expect);
      });
    });

    it('"here and there" should end with here', function() { //trap since it starts and ends with 'here'
      expect( 'here and there'.endsWith('here') ).toBe(true);
    });

    it('bob should not ends with bob2', function() {
      expect( 'bob'.endsWith('bob2') ).toBe(false);
    });

  });

  describe("stripHtml (requirement6)", function() {

    it('Should get html stripped out', function() {
      expect( '<p>Shoplifters of the World <em>Unite</em>!</p>'.stripHtml() ).toEqual('Shoplifters of the World Unite!');
    });

    it('Should remain as is', function() {
      var input = '1 &lt; 2';
      expect( input.stripHtml() ).toEqual(input);
    });

  });

});