describe("Wopen", function() {

  var popup;

  beforeEach(function () {
    document.getElementById('wrapper').innerHTML = $("#wopenTemplate").html();

    spyOn(window, 'open');
  });

  afterEach(function () {
    document.getElementById('wrapper').innerHTML = '';
  });

  it('Window.open called on click', function() {
    $('a').wopen();

    var params = $('a').first().trigger('click');
    // console.log(params);

    expect(window.open).toHaveBeenCalled(); //, '_blank', 'resizable'
  });

  it('Window.open called with data-window-group and default options', function() {
    var $link = $('a').first();
    $('a').wopen();

    var params = $link.trigger('click');
    // console.log(params);

    expect(window.open).toHaveBeenCalledWith($link.attr('href'), 'personal', 'height=500,width=500,');
  });

  it('Window.open called with options', function() {
    var $link = $('a').last();

    $link.wopen({
      height:800,
      menubar: 'no'
    });

    $link.trigger('click');

    expect(window.open).toHaveBeenCalledWith($link.attr('href'), '', 'height=800,width=500,menubar=no,');
  });

  it('Window.open not called', function() {
    var $link = $('a').eq(1);

    $link.trigger('click');

    expect(window.open).wasNotCalled();
  });

});
