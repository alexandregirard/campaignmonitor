;(function(){
  'use strict';

  //main controller of the app
  //pulling the data that are feeding the cm-issues directive
  app.controller('AppCtrl', ['$scope', '$timeout', 'cmIssuesFactory',  function ($scope, $timeout, cmIssuesFactory) {

    $scope.loading = false;
    $scope.error   = '';

    $scope.fetch = function(){
      $scope.loading = true;

      cmIssuesFactory.fetch()
      .success(function(data){
        $timeout(function(){
          $scope.data    = data || [];
          $scope.loading = false;
          //console.log('data', data);
        },1000);
      })
      .error(function(){
        console.log('error', arguments);
        $scope.error = 'Failed to load the issues. Please try again.';
        $scope.loading = false;
      });
    };

    $scope.closeError = function(){
      $scope.error = '';
    };

  }]);


})();