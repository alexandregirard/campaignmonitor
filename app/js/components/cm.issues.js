(function(){
  'use strict';

  var module = angular.module('cm.issues', []);

  // angular constraints is to set the token callback to JSON_CALLBACK and then replace it internally (check source)
  // to ensure parralel processing without spoiling the global scope with "window.cb" or the kind
  //
  // SO
  // for the sake of dev, I had to edit test.jsonp and change the wrapping function name
  // but ideally the server is suppose to accept the parameter "callback" as the convention mentions
  module.factory('cmIssuesFactory', ['$http',
    function ($http) {

    return {
      fetch: function(){
        return $http.jsonp('test.jsonp?callback=JSON_CALLBACK'); //return a promise
      }
    };

  }]);

  //directive dedicated to display issues (not really necessary... a showcase)
  module.directive('cmIssues', [
    function(){
      return {
        scope:{
          issues: '='
        },
        templateUrl: 'app/pages/issue.html'
      };
    }
  ]);

})();