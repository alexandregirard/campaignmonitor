(function(window){'use strict';
  window.CFG = {
    APP:{
      APP_NAME: '/* @echo APP_NAME */',
      APP_VERSION: '/* @echo APP_VERSION */',
      LOG: /* @echo LOG */
    },
    LANG: 'en_UK',
    API: {
      // @if NODE_ENV == 'DEV'
      URI: 'http:///* @echo IP_ADDRESS */:8081',
      // @endif
      // @if NODE_ENV == 'STAGE'
      //URI: 'http://testing-70154.apse1.nitrousbox.com/'
      URI: 'http://staging.hipages.com.au'
      // @endif
      // @if NODE_ENV == 'LIVE'
      URI: 'http://api.hipages.com.au'
      // @endif
    },
    GOOGLE: {
      // @if NODE_ENV == 'DEV' || NODE_ENV == 'STAGE'
      PROJECTID: '',
      GMAP: '',
      ANALYTICS: ''
      // @endif
      // @if NODE_ENV == 'LIVE'
      PROJECTID: '',
      GMAP: '',
      ANALYTICS: ''
      // @endif
    },
    FB: {//FB app credentials
      APP_NAME: '',
      APP_ID: ''
    },
    TF: {} //testflight
  };
})(window);