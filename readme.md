#### Campaign "hire me" monitor

###Requirements 5, and 6 

Source: /src/String.js
Tests: open /SpecRunner.html


###Requirements 7

Source: jquery.wopen.js
Tests: open /SpecRunner.html


###Requirements 8

####Install

Assuming you have npm, grunt and bower installed.

* Pull repo
* Execute npm install
* Execute bower install


####Develop

* Execute "grunt"
* Open browser to http://localhost:8081/requirement8.html


####Tools

* jQuery
* SASS
* Angular (with animate)
* Heavy usage of grunt, because I ain't do 1 millions (even 1k actually) the same thing


####Insights

* When developing, grunt watch over any changes done on sass files, config/config.tmpl.js, and angular partials
* Grunt serves the / folder through http


###Notes

* Dev test for angular
* Add a filter (based on angular-moment ?) computing the update date
* Clear the mess at the root level and generate an app/ folder with bower, css, js...
