/*
 * jQuery plugin wopen
 * description: change the behaviour of link
 * dependancy: jquery
 */
;(function($){
  'use strict';

  if(typeof $.fn.wopen==='function'){
    console.warn('jQuery plugin collision - overloading $.fn.wopen');
  }

  $.fn.wopen = function(options){

    options = options || {};

    var defaultProps = {
      strUrl            : '',
      strWindowName     : '',
      strWindowFeatures : {
        height : 500,
        width  : 500
      }
    };

    //given a collection of jquery object, we apply the plugin on anchor tags only
    return this.filter('a').each(function(){

      var $this = $(this); //caching jquery object

      $this.on('click', function(e){

        //prevent default anchor behaviour
        e.preventDefault();

        //convert the options object as a string formatted as '{{optionKey}}={{optionValue}},''
        var newOptions = $.extend(defaultProps.strWindowFeatures, options);
        var strWindowFeatures = '';
        $.each(newOptions, function(key, value){
          strWindowFeatures+=key+'='+value+',';
        });
        // console.log('strWindowFeatures' , strWindowFeatures);
        // return;

        //extend properties with given options
        //note: strWindowFeatures is a string of properties applied to the window, and since the last value is taken, we simply need to concat the default strWindowFeatures to the given one.
        var props = $.extend(null, defaultProps, {
          strUrl            : $this.attr('href'),
          strWindowName     : $this.attr('data-­window­-group'),
          strWindowFeatures : strWindowFeatures
        });

        //opening up the window
        var windowObjectReference = window.open(props.strUrl, props.strWindowName, props.strWindowFeatures);
      });
    });
  };

})(jQuery);