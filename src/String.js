/*
 * file: String.js
 * description: Requirement 5 and 6 - extending string prototype. Any module specific var stay isolated (note: none needed yet)
 * date: 20140903
 * author: alex
 */

;(function(){
  'use strict';

  /*
   * function: startsWith
   * params: string (typeof 'string')
   * description: Solution based on indexOf. We check that this index is 0, meaning there's not even 1 string before
   */
  if(!String.prototype.startsWith){
    Object.defineProperty(String.prototype, 'startsWith', {
      value: function(string){
        return typeof string==='string' && !!~this.indexOf(string) && this.indexOf(string)===0;
      },
      enumerable: false
    });
  }

  /*
   * function: endsWith
   * params: string (typeof 'string')
   * description: Solution based on lastIndexOf. Given that the last occurency position of string, we add the string length and it should match the overall length of this
   * note: a regex might be a good alternative
   */
  if(!String.prototype.endsWith){
    Object.defineProperty(String.prototype, 'endsWith', {
      value: function(string){
        return typeof string==='string' && !!~this.lastIndexOf(string) && this.length === this.lastIndexOf(string)+string.length;
      },
      enumerable: false
    });
  }

  /*
   * function: stripHtml
   * params: none
   * description: Solution based on regex. Assumption: an html/xml tags is basically at least 1 character wrapped inside '<' and '>'. this wrapped character is not '>'. We replace with an empty string "" every occurencies (flag g) regardless of case (flag i) of the matched tags
   */
  if(!String.prototype.stripHtml){
    Object.defineProperty(String.prototype, 'stripHtml', {
      value: function(){
        return this.replace(/(<([^>]+)>)/ig, '');
      },
      enumerable: false
    });
  }

})();