;(function($){
  'use strict';

  window.app = angular.module('myApp', []);

  // app.constant('CFG', config);

  // app.constant('API', {
  //   url: config.API.URI,
  //   params2query: function(obj){
  //     return Object.keys(obj).map(function(k) {
  //       return encodeURIComponent(k) + '=' + encodeURIComponent(obj[k]);
  //     }).join('&');
  //   }
  // });

  angular.element(document).ready(function() {
    angular.bootstrap(document, ['myApp']);
  });

})();